publish:
	docker build -t registry.gitlab.com/thelinm/24hr-deploy-machine/motor ./services/motor
	docker build -t registry.gitlab.com/thelinm/24hr-deploy-machine/frontend ./services/frontend
	docker push registry.gitlab.com/thelinm/24hr-deploy-machine/motor
	docker push registry.gitlab.com/thelinm/24hr-deploy-machine/frontend

prod:
	docker-compose -f docker-compose.example.yml pull
	docker-compose -f docker-compose.example.yml up -d

start:
	docker-compose up -d

stop:
	docker-compose down